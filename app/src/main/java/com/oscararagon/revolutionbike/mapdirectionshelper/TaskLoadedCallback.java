package com.oscararagon.revolutionbike.mapdirectionshelper;

/**
 * Created by Vishal on 10/20/2018.
 */

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
