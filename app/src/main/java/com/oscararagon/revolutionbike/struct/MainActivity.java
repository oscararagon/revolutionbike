package com.oscararagon.revolutionbike.struct;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.oscararagon.revolutionbike.R;
import com.oscararagon.revolutionbike.classes.UserLogged;

public class MainActivity extends AppCompatActivity {
    private BottomNavigationView bottomNav;
    private ImageView btnSettings;
    private UserLogged firstCheck;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {


        firstCheck = new UserLogged(getApplicationContext());

        String sharedUsername = firstCheck.getUsername();
        int sharedUserID = firstCheck.getUserID();
        if(sharedUsername == null || sharedUserID == 0){
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        }


        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);




        bottomNav = findViewById(R.id.bottom_nav);
        btnSettings = findViewById(R.id.btnSettings);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Settings.class);
                startActivity(intent);
            }
        });


        if(savedInstanceState == null){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            switch(item.getItemId()){
                case R.id.nav_health:
                    selectedFragment = new HealthFragment();
                    break;
                case R.id.nav_report:
                    selectedFragment = new ReportFragment();
                    break;
                case R.id.nav_track:
                    selectedFragment = new HomeFragment();
                    break;
                case R.id.nav_account:
                    selectedFragment = new AccountFragment();
                    break;
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectedFragment).commit();
            return true;
        }
    };
}


