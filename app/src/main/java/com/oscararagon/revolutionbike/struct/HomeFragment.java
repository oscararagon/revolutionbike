package com.oscararagon.revolutionbike.struct;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.oscararagon.revolutionbike.R;

public class HomeFragment extends Fragment {

    protected Button goals, track;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View homeView = inflater.inflate(R.layout.fragment_home, container, false);

        track = homeView.findViewById(R.id.reportTrack);
        goals = homeView.findViewById(R.id.reportGoal);

        track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentTrack = new Intent(getActivity(), MapsActivity.class);
                startActivity(intentTrack);
            }
        });

        goals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentGoals = new Intent(getActivity(), GoalsActivity.class);
                intentGoals.putExtra("FromMap", false);
                startActivity(intentGoals);
            }
        });

        return homeView;
    }
}
