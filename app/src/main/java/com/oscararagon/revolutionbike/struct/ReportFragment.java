package com.oscararagon.revolutionbike.struct;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.oscararagon.revolutionbike.R;
import com.oscararagon.revolutionbike.classes.UserLogged;
import com.oscararagon.revolutionbike.lib.HTTPConnectionClass;
import com.oscararagon.revolutionbike.lib.ReportAdapter;
import com.oscararagon.revolutionbike.lib.ResponseCallback;

import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ReportFragment extends Fragment{

    private ListView reportList;
    private TextView listEmpty;

    private String STATIC_MAP_API_ENDPOINT = "https://maps.googleapis.com/maps/api/staticmap?size=230x200&path=";

    private boolean scheduledReportSelected;
    private Spinner statoViaggi;
    private UserLogged user;
    private String URL_GET_VIAGGI_IMPOSTATI, URL_GET_VIAGGI_FINITI, URL_DELETE_REPORT, URL_EDIT_REPORT;
    private ArrayList<Bundle> reports;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View reportView = inflater.inflate(R.layout.fragment_report, container, false);

        reports = new ArrayList<>();
        user = new UserLogged(getContext());

        statoViaggi = reportView.findViewById(R.id.statoViaggi);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.tipoViaggio, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statoViaggi.setAdapter(adapter);
        reportList = reportView.findViewById(R.id.listReport);
        listEmpty = reportView.findViewById(R.id.listEmpty);
        reportList.setEmptyView(listEmpty);

        statoViaggi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                if(position == 0){ //carico i viaggi impostati
                    scheduledReportSelected = true;
                    URL_GET_VIAGGI_IMPOSTATI = "https://revolutionbike.it/android_connection/getReports.php?CodPersona="+user.getUserID()+"&Stato=0";
                    //metto l'url dentro un array list di Bundle.
                    ArrayList<Bundle> UrlGetReports = new ArrayList<>();
                    Bundle url = new Bundle();
                    url.putString("URL", URL_GET_VIAGGI_IMPOSTATI);
                    UrlGetReports.add(url);
                    new HTTPConnectionClass(new ResponseCallback() {
                        @Override
                        public void onRespond(ArrayList<Bundle> values) {
                            if(values.get(0).containsKey("msg")){
                                listEmpty.setText( values.get(0).getString("msg"));
                                values.clear();
                                ReportAdapter adapter = new ReportAdapter(getContext(), R.layout.report_item_layout, values);
                                reportList.setAdapter(adapter);
                            }else {
                                //adapter dei report impostati nella lista
                                reports = values;
                                ReportAdapter adapter = new ReportAdapter(getContext(), R.layout.report_item_layout, values);
                                reportList.setAdapter(adapter);
                            }
                        }
                    }).execute(UrlGetReports);
                }else{ //carico i viaggi terminati
                    scheduledReportSelected = false;
                    URL_GET_VIAGGI_FINITI = "https://revolutionbike.it/android_connection/getReports.php?CodPersona="+user.getUserID()+"&Stato=4";
                    //metto l'url dentro un array list di Bundle.
                    ArrayList<Bundle> UrlGetReports = new ArrayList<>();
                    Bundle url = new Bundle();
                    url.putString("URL", URL_GET_VIAGGI_FINITI);
                    UrlGetReports.add(url);
                    new HTTPConnectionClass(new ResponseCallback() {
                        @Override
                        public void onRespond(ArrayList<Bundle> values) {
                            //adapter dei report impostati nella lista
                            if(values.get(0).containsKey("msg")){
                                listEmpty.setText( values.get(0).getString("msg"));
                                values.clear();
                                ReportAdapter adapter = new ReportAdapter(getContext(), R.layout.report_item_layout, values);
                                reportList.setAdapter(adapter);
                            }else{
                                reports = values;
                                ReportAdapter adapter = new ReportAdapter(getContext(), R.layout.report_item_layout, values);
                                reportList.setAdapter(adapter);
                            }
                        }
                    }).execute(UrlGetReports);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        reportList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle report = reports.get(i);
                report.putInt("NumReport", i + 1);
                Intent intentReport = new Intent(getActivity().getApplicationContext(), ReportDetailActivity.class);
                intentReport.putExtras(report);
                startActivity(intentReport);
            }
        });


        /**
         * Do la possibilità di cancellare e eliminare un report impostato
         **/
        reportList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if(scheduledReportSelected){
                    Bundle report = reports.get(position);
                    new AlertDialog.Builder(getActivity()).setTitle("Modifica o cancella il tuo report")
                            .setPositiveButton("Modifica", new DialogInterface.OnClickListener() {
                                /**
                                 * Rimando all'activity dell'impostazione dei dati fitness
                                 * */

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent editReportIntent = new Intent(getActivity().getApplicationContext(), GoalsActivity.class);
                                    editReportIntent.putExtra("toEdit", report);
                                    startActivity(editReportIntent);
                                }
                            })
                            .setNegativeButton("Elimina", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ArrayList<Bundle> UrlDeleteReport = new ArrayList<>();
                                    URL_DELETE_REPORT = "https://revolutionbike.it/android_connection/deleteReport.php?CodPersona="+user.getUserID()+
                                            "&IdReport="+report.getString("Id");
                                    Bundle url = new Bundle();
                                    url.putString("URL", URL_DELETE_REPORT);
                                    UrlDeleteReport.add(url);
                                    new HTTPConnectionClass(values -> {
                                        //in values.get(0) ci sarà il messaggio di avvenuto o meno successo dell'impostazione del viaggio
                                        Toast.makeText(getApplicationContext(), values.get(0).getString("msg"), Toast.LENGTH_SHORT).show();
                                    }).execute(UrlDeleteReport);
                                }
                            }).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Non puoi modificare o eliminare un viaggio già terminato", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        return reportView;
    }

     /*values è una lista di bundle.
        struttura di ogni bundle:
        [ "Id",
          "Titolo",
          "Data",
          "PartenzaLat",
          "PartenzaLng",
          "ArrivoLat",
          "ArrivoLng",
          "Tempo",
          "KmPercorsi"
        ]*/
}
