package com.oscararagon.revolutionbike.struct;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import android.view.View;
import android.widget.Button;

import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.oscararagon.revolutionbike.R;
import com.oscararagon.revolutionbike.lib.ResponseCallback;
import com.oscararagon.revolutionbike.mapdirectionshelper.FetchURL;
import com.oscararagon.revolutionbike.mapdirectionshelper.TaskLoadedCallback;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, ResponseCallback, TaskLoadedCallback {

    private GoogleMap mMap;
    private CameraPosition mCameraPosition;
    private MarkerOptions partenza, arrivo;
    private Polyline currentPolyline;
    private Place startPlace, endPlace;
    private ArrayList<LatLng> points;



    //variabili settaggio Map e Places
    private TextView startTrack, endTrack;
    private Button btnGoals;
    private boolean startSelected, endSelected;
    private boolean startTextViewClicked, endTextViewClicked;
    private PlacesClient placesClient;
    private Location mLastKnownLocation;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationCallback locationCallback;
    private final LatLng mDefaultLocation = new LatLng(44.843910, 10.728610);


    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final int AUTOCOMPLETE_REQUEST_CODE = 10;
    private static final String TAG = MapsActivity.class.getSimpleName();
    private boolean mLocationPermissionGranted; //per i permessi di posizione

    //valori chiave
    private static final int DEFAULT_ZOOM = 15;
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);

        getSupportActionBar().hide();

        //setto la lista dei punti di LatLng del percorso che setterò
        points = new ArrayList<>();

        startTrack = (TextView) findViewById(R.id.start);
        endTrack = (TextView) findViewById(R.id.end);
        btnGoals = (Button) findViewById(R.id.btnGoals);


        startSelected = false;
        endSelected = false;

        //questi due bool mi servono per capire se devo settare la partenza o l'arrivo
        startTextViewClicked = false;
        endTextViewClicked = false;

        btnGoals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentGoals = new Intent(MapsActivity.this, GoalsActivity.class);
                Bundle datiViaggio = new Bundle();
                Bundle puntiPolyline = new Bundle();
                datiViaggio.putBoolean("FromMap", true);
                datiViaggio.putString("LatLngPartenza", startPlace.getLatLng().latitude+","+startPlace.getLatLng().longitude);
                datiViaggio.putString("StrPartenza", startPlace.getName());
                datiViaggio.putString("LatLngArrivo", endPlace.getLatLng().latitude+","+endPlace.getLatLng().longitude);
                datiViaggio.putString("StrArrivo", endPlace.getName());
                intentGoals.putExtras(datiViaggio);
                //salvo su un bundle i lat lon
                for(int i = 0; i < points.size(); i++)
                    puntiPolyline.putString("LatLng"+i, points.get(i).longitude+" "+points.get(i).latitude);

                intentGoals.putExtra("puntiPolyline", puntiPolyline);
                startActivity(intentGoals);
            }
        });

        //inizializzo il client che prenderà la mia posizione corrente e il client per i luoghi
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.google_maps_key), Locale.ITALY);
        }
        placesClient = Places.createClient(this);


        if(savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }




    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        //quando l'activity è in pausa mi salvo lo stato della mappa
        if(mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        //attivo il gps se non ancora attivo
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

        SettingsClient settingsClient = LocationServices.getSettingsClient(MapsActivity.this);
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());

        task.addOnSuccessListener(MapsActivity.this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                getDeviceLocation();
            }
        });

        task.addOnFailureListener(MapsActivity.this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    try {
                        resolvable.startResolutionForResult(MapsActivity.this, 51);
                    } catch (IntentSender.SendIntentException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });


        //chiedo all'utente il permesso per l'uso della mappa
        getLocationPermission();

        //visualizzo il layer del Turn On My Location
        updateLocationUI();

        //ottengo la posizione corrente del device e sposto la mappa lì
        getDeviceLocation();
    }

    //settaggio di partenza e arrivo
    public void startAutoCompleteActivity(View view){
        if(view.getId() == R.id.start){
            startTextViewClicked = true;
        }else if(view.getId() == R.id.end){
            endTextViewClicked = true;
        }
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY,
                Arrays.asList(Place.Field.NAME, Place.Field.LAT_LNG))
                .setCountry("IT")
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 51) {
            if (resultCode == RESULT_OK) {
                getDeviceLocation();
            }
        }


        if(requestCode == AUTOCOMPLETE_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                Place place = Autocomplete.getPlaceFromIntent(data);
                if(startTextViewClicked){
                    startTrack.setText(place.getName());
                    if((!startSelected && endSelected) || (startSelected && endSelected)){
                        startSelected = true;
                        startPlace = place;
                        setPercorso(startPlace, endPlace);
                    }else if((!startSelected && !endSelected) || (startSelected && !endSelected)){
                        startPlace = place;
                        startSelected = true;
                    }
                    startTextViewClicked = false;
                }
                if(endTextViewClicked){
                    endTrack.setText(place.getName());
                    if((!endSelected && startSelected) || (endSelected && startSelected)) {
                        endSelected = true;
                        endPlace = place;
                        btnGoals.setEnabled(true);
                        setPercorso(startPlace, endPlace);
                    }else if((!endSelected && !startSelected) || (endSelected && !startSelected)){
                        endSelected = true;
                        endPlace = place;
                    }
                    endTextViewClicked = false;
                }
            }else if(resultCode == AutocompleteActivity.RESULT_ERROR){
                Status s = Autocomplete.getStatusFromIntent(data);
                Log.i("STATUS_ERROR", s.getStatusMessage());
            }
        }
    }


    /**
    * Ottengo la posizione corrente del device e animo la mappa in quella posizione
    * */
    //@SuppressLint("MissingPermission")
    private void getDeviceLocation() {
        /**
         * Prende la posizione corrente più accurata possibile.
         * se è nulla prende una posizione di default (Novellara)
         */
        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation()
                        .addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Setto la posizione corrente
                            mLastKnownLocation = task.getResult(); //PROBLEMA!!! POSIZIONE NON PRESA
                            if (mLastKnownLocation != null) {
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                        new LatLng(mLastKnownLocation.getLatitude(),
                                                mLastKnownLocation.getLongitude()), DEFAULT_ZOOM ));
                            }else{
                                final LocationRequest locationRequest = LocationRequest.create();
                                locationRequest.setInterval(10000);
                                locationRequest.setFastestInterval(5000);
                                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                                locationCallback = new LocationCallback() {
                                    @Override
                                    public void onLocationResult(LocationResult locationResult) {
                                        super.onLocationResult(locationResult);
                                        if (locationResult == null) {
                                            return;
                                        }
                                        mLastKnownLocation = locationResult.getLastLocation();
                                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                                        mFusedLocationProviderClient.removeLocationUpdates(locationCallback);
                                    }
                                };

                                mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Current location is null. Using defaults.", Toast.LENGTH_SHORT).show();
                            Log.e(TAG, "Exception: %s", task.getException());
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM ));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    /**
    * Mostro all'utente il layer di richiesta per del permesso dell'utilizzo della posizione
    * */
    private void getLocationPermission(){

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
    * Analizzo la risposta della richiesta per il permesso alla posizione
    * */

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    /**
     * a seconda che l'utente abbia dato il permesso o meno, mostro la sua posizione
     */
    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);                                          // pulsanti + e - dello zoom
                mMap.getUiSettings().setCompassEnabled(true);                                               // bussola
                mMap.getUiSettings().setZoomGesturesEnabled(true);                                          // gestire lo zoom con dita
                mMap.getUiSettings().setRotateGesturesEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    public void setPercorso(Place start, Place end){

        partenza = new MarkerOptions().position(start.getLatLng());
        arrivo = new MarkerOptions().position(end.getLatLng());

        mMap.clear();

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(partenza.getPosition());
        builder.include(arrivo.getPosition());
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 100));
        mMap.addMarker(partenza);
        mMap.addMarker(arrivo);
        new FetchURL(MapsActivity.this).execute(getUrl(start.getLatLng(), end.getLatLng(), "walking"), "walking");
    }

    public String getUrl(LatLng origin, LatLng dest, String dirMode){
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String mode = "mode=" + dirMode;

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/json?" + parameters + "&key=" + getString(R.string.google_maps_key);
        return url;
    }

    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null)
            currentPolyline.remove();
        PolylineOptions po = (PolylineOptions) values[0];
        //prelevo la lat e lon di ogni punto della polyline
        for(int i = 0; i < po.getPoints().size(); i++){
            points.add(po.getPoints().get(i));
        }
        currentPolyline = mMap.addPolyline((PolylineOptions) values[0]);
    }

    @Override
    public void onRespond(ArrayList<Bundle> values) {

    }
}
