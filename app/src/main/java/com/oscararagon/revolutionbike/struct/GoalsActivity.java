package com.oscararagon.revolutionbike.struct;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.textfield.TextInputEditText;
import com.oscararagon.revolutionbike.lib.HTTPConnectionClass;
import com.oscararagon.revolutionbike.R;
import com.oscararagon.revolutionbike.classes.Report;
import com.oscararagon.revolutionbike.lib.ResponseCallback;
import com.oscararagon.revolutionbike.classes.UserLogged;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

public class GoalsActivity extends AppCompatActivity {

    TextInputEditText titoloReport;
    private EditText reportKm, reportKcal;
    private TextView  moreKm, subKm, moreKcal, subKcal;
    private NumberPicker hours, minutes, seconds;
    private LinearLayout setKm;
    private ImageView btnSchedule;
    private ProgressBar progressBar;
    private File fRoute;
    private boolean fromMap;
    private Bundle punti;

    private UserLogged user;

    private String URL_SCHEDULE_REPORT;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goals);

        getSupportActionBar().hide();


        Intent intent = this.getIntent();
        user = new UserLogged(this);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        btnSchedule = (ImageView) findViewById(R.id.btnDone);
        btnSchedule.setOnClickListener(scheduleReport);

        titoloReport = (TextInputEditText) findViewById(R.id.titoloReport);
        setKm = (LinearLayout) findViewById(R.id.reportSetKm);
        reportKm = findViewById(R.id.reportKm);
        reportKm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                boolean handled = false;

                if(actionId == EditorInfo.IME_ACTION_NEXT){
                    handled = checkEditTextValue(reportKm);
                    reportKcal.requestFocus();
                }
                return handled;
            }
        });


        reportKcal = findViewById(R.id.reportKcal);
        reportKcal.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                boolean handled = false;
                if(actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    handled = checkEditTextValue(reportKcal);
                    imm.hideSoftInputFromWindow(textView.getWindowToken(), 0); //nascondo la tastiera
                }
                return handled;
            }
        });

        moreKm = findViewById(R.id.btnMoreKm);
        moreKm.setOnClickListener(btnListener);
        subKm = findViewById(R.id.btnLessKm);
        subKm.setOnClickListener(btnListener);
        moreKcal = findViewById(R.id.btnMoreKcal);
        moreKcal.setOnClickListener(btnListener);
        subKcal = findViewById(R.id.btnLessKcal);
        subKcal.setOnClickListener(btnListener);

        hours = findViewById(R.id.targetH);
        hours.setMinValue(0);
        hours.setMaxValue(12);
        hours.setWrapSelectorWheel(true);

        minutes = findViewById(R.id.targetM);
        minutes.setMinValue(0);
        minutes.setMaxValue(59);
        minutes.setWrapSelectorWheel(true);

        seconds = findViewById(R.id.targetS);
        seconds.setMinValue(0);
        seconds.setMaxValue(59);
        seconds.setWrapSelectorWheel(true);

        if(intent != null){
            fromMap = intent.getExtras().getBoolean("FromMap");
            if(fromMap){ //se sono passato da MapsActivity, allora creo il file txt dei punti di lat e lon per navit
                setKm.setVisibility(View.GONE);
                punti = intent.getBundleExtra("puntiPolyline");
            }
            Bundle reportToEdit = intent.getBundleExtra("toEdit");
            if (reportToEdit != null){
                titoloReport.setText(reportToEdit.getString("Titolo"));
                reportKm.setText(reportToEdit.getString("KmSetted"));
                reportKcal.setText(reportToEdit.getString("KcalSetted"));
            }
        }
    }

    /**
    * Metodo che controlla i valori dell'EditText che passo come parametro
    * Mi ritorna true se ho dovuto modificare il valore dell'EditText passato,
    * false altrimenti. Questo valore di ritorno è particolarmente utile quando
    * lo chiama il bottone di conferma: prima di mandare i dati al db verifica
    * che i valori dei due EditText siano validi, altrimenti manda un messaggio all'utente
    * */

    private boolean checkEditTextValue(EditText et){
        String etToString = et.getText().toString();
        boolean updated = false; //inizialmente falso il valore di updated
        switch(et.getId()){
            case R.id.reportKm:
                if(etToString.equals("") || (Float.parseFloat(etToString) < 0.5)){
                    Toast.makeText( getApplicationContext() , "Inserire un valore tra 0.5 km e 999.0 km", Toast.LENGTH_SHORT).show();
                    et.setText("0.5");
                    updated = true;
                } else if(Float.parseFloat(etToString) > 999.0){
                    Toast.makeText( getApplicationContext() , "Inserire un valore tra 0.5 km e 999.0 km", Toast.LENGTH_SHORT).show();
                    et.setText("999.0");
                    updated = true;
                }else if(!etToString.contains(".")){
                    if(!etToString.equals(String.valueOf(Integer.parseInt(etToString)))){
                        et.setText(String.valueOf(Integer.parseInt(etToString)));
                        updated = true;
                    }else et.setText(String.valueOf(Integer.parseInt(etToString)));
                }else if(!etToString.equals(String.valueOf(Float.parseFloat(etToString)))){
                        et.setText(String.valueOf(Float.parseFloat(etToString)));
                        updated = true;
                }else et.setText(String.valueOf(Integer.parseInt(etToString)));
                break;

            case R.id.reportKcal:
                if(etToString.equals("") || (Integer.parseInt(etToString) < 50)){
                    Toast.makeText( getApplicationContext() , "Inserire un valore tra 50 kcal e 9950 kcal", Toast.LENGTH_SHORT).show();
                    et.setText("50");
                    updated = true;
                } else if(Integer.parseInt(etToString) > 9950){
                    Toast.makeText( getApplicationContext() , "Inserire un valore tra 50 kcal e 9950 kcal", Toast.LENGTH_SHORT).show();
                    et.setText("9950");
                    updated = true;
                } else if(!etToString.equals(String.valueOf(Integer.parseInt(etToString)))){
                    et.setText(String.valueOf(Integer.parseInt(etToString)));
                    updated = true;
                }else et.setText(String.valueOf(Integer.parseInt(etToString)));

                break;
        }
        return updated;
    }


    private TextView.OnClickListener btnListener = new TextView.OnClickListener(){

        @Override
        public void onClick(View view) {

            float distance;
            int kcal;

            switch (view.getId()){
                case R.id.btnMoreKm:
                    distance = Float.parseFloat(reportKm.getText().toString());
                    if (distance >= 999.0){
                        Toast.makeText(view.getContext(), "Inserire un valore tra 0.5 km e 999.0 km", Toast.LENGTH_SHORT).show();
                        reportKm.setText("999.0");
                    }else{
                        distance += 0.5;
                        reportKm.setText(Float.toString(distance));
                    }
                    break;
                case R.id.btnLessKm:
                    distance = Float.parseFloat(reportKm.getText().toString());
                    if (distance <= 0.5){
                        Toast.makeText(view.getContext(), "Inserire un valore tra 0.5 km e 999.0 km", Toast.LENGTH_SHORT).show();
                        reportKm.setText("0.5");
                    }else{
                        distance -= 0.5;
                        reportKm.setText(Float.toString(distance));
                    }
                    break;
                case R.id.btnMoreKcal:
                    kcal = Integer.parseInt(reportKcal.getText().toString());
                    if (kcal >= 9950){
                        Toast.makeText(view.getContext(), "Inserire un valore tra 50 kcal e 9950 kcal", Toast.LENGTH_SHORT).show();
                        reportKcal.setText("9950");
                    }else{
                        kcal += 50;
                        reportKcal.setText(Integer.toString(kcal));
                    }
                    break;
                case R.id.btnLessKcal:
                    kcal = Integer.parseInt(reportKcal.getText().toString());
                    if (kcal <= 50){
                        Toast.makeText(view.getContext(), "Inserire un valore tra 50 kcal e 9950 kcal", Toast.LENGTH_SHORT).show();
                        reportKcal.setText("50");
                    }else{
                        kcal -= 50;
                        reportKcal.setText(Integer.toString(kcal));
                    }
                    break;
            }
        }
    };

    private ImageView.OnClickListener scheduleReport = new View.OnClickListener(){

        @Override
        public void onClick(View view) {

            //prima nascondo la tastiera nel caso sia aperta
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0); //nascondo la tastiera

            //Imposto i valori dei report e salvo su db
            if(!((titoloReport.getText().toString()).isEmpty())) { //se è stato inserito un titolo al report, allora ok

                ArrayList<Bundle> reportToSave = new ArrayList<>();
                Bundle reportData = new Bundle();

                if(fromMap && !checkEditTextValue(reportKcal)) { //se arrivo dall'impostazione del percorso da mappa e le kcal sono corrette, salvo le coordinate
                    String[] latlogP = getIntent().getExtras().getString("LatLngPartenza").split(",");
                    String[] latlogA = getIntent().getExtras().getString("LatLngArrivo").split(",");
                    LatLng latlngstart = new LatLng(Double.parseDouble(latlogP[0]), Double.parseDouble(latlogP[1]));
                    LatLng latlogend = new LatLng(Double.parseDouble(latlogA[0]), Double.parseDouble(latlogA[1]));
                    Report viaggio = new Report(titoloReport.getText().toString(), getIntent().getExtras().getString("StrPartenza"), getIntent().getExtras().getString("StrArrivo"),
                            latlngstart, latlogend, Float.parseFloat(reportKcal.getText().toString()), "00:00:00");
                    URL_SCHEDULE_REPORT = "https://www.revolutionbike.it/android_connection/scheduleReport.php?CodPersona=" +user.getUserID()+
                            "&titolo="+viaggio.getTitolo()+
                            "&strPartenza="+ viaggio.getStrPartenza()+
                            "&strArrivo="+viaggio.getStrArrivo()+
                            "&latPartenza="+viaggio.getPartenza().latitude+
                            "&lonPartenza="+viaggio.getPartenza().longitude+
                            "&latArrivo="+viaggio.getArrivo().latitude+
                            "&lonArrivo="+viaggio.getArrivo().longitude+
                            "&kcalSetted="+viaggio.getKcal()+
                            "&kmSetted=0.0"+
                            "&durataSetted="+viaggio.getDurata()+
                            "&data="+viaggio.getCurrentDate();
                    reportData.putString("URL", URL_SCHEDULE_REPORT);
                    /**
                     *upload del file delle coordinate sul server
                     */
                    fRoute = new File(getFilesDir(),"route"+titoloReport.getText().toString()+".txt");
                    try {
                        //creato il nuovo file. Ora scrivo il suo contenuto (i punti di latitudine e longitudine)
                        FileWriter fw = new FileWriter(fRoute);
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write("type=street_route\n");
                        for(int i = 0; i < punti.size(); i++){
                            bw.write(punti.getString("LatLng"+i)+"\n");
                        }
                        bw.close();
                        fw.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    progressBar.setVisibility(View.VISIBLE);
                    new uploadFile().execute(fRoute.getPath(), fRoute.getName());
                }else{
                    boolean kmValid = checkEditTextValue(reportKm);
                    boolean kcalValid = checkEditTextValue(reportKcal);
                    if(!kmValid && !kcalValid) { //se nessuno dei due EditText è stato modificato, allora ok
                        String durata =  hours.getValue()+":"+minutes.getValue()+":"+seconds.getValue();
                        Report viaggio = new Report(titoloReport.getText().toString(), Float.parseFloat(reportKm.getText().toString()),Float.parseFloat(reportKcal.getText().toString()), durata);
                        URL_SCHEDULE_REPORT = "https://www.revolutionbike.it/android_connection/scheduleReport.php?CodPersona="+user.getUserID()+
                                "&titolo="+viaggio.getTitolo()+
                                "&kmSetted="+viaggio.getKm()+
                                "&kcalSetted="+viaggio.getKcal()+
                                "&durataSetted="+viaggio.getDurata()+
                                "&data="+viaggio.getCurrentDate();
                        reportData.putString("URL", URL_SCHEDULE_REPORT);
                    }else Toast.makeText(GoalsActivity.this, "Attenzione, c'erano dei valori di Km e Kcal non validi! Modificati", Toast.LENGTH_SHORT).show();
                }
                reportToSave.add(reportData);
                new HTTPConnectionClass(new ResponseCallback() {
                    @Override
                    public void onRespond(ArrayList<Bundle> values) {
                    //in values.get(0) ci sarà il messaggio di avvenuto o meno successo dell'impostazione del viaggio
                        Toast.makeText(getApplicationContext(), values.get(0).getString("msg"), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(GoalsActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                }).execute(reportToSave);
            }else Toast.makeText(getApplicationContext(), "Inserisci un titolo al tuo viaggio", Toast.LENGTH_SHORT).show();
        }
    };


    /**
     * classe asincrona per l'upload del file con la route
     * sul server.doInBackground prende come parametro il percorso assoluto del file
     * */
    
    public class uploadFile extends AsyncTask<String, String, String>{

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setVisibility(View.GONE);
            Log.e("ServerCodeResponse", "Server code: "+s);
        }

        @Override
        protected String doInBackground(String... strings) {

            String filename = strings[0];
            String serverResponseCode = "0";

            String uploadServerUri = "https://www.revolutionbike.it/android_connection/uploadToServer.php";
            HttpURLConnection conn = null;
            DataOutputStream dos = null;
            //multipart form data
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";

            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;
            File sourceFile = new File(filename);
            if(!sourceFile.isFile()){
                progressBar.setVisibility(View.GONE);
                Toast.makeText(GoalsActivity.this, "Couldn't upload file because file "+filename+" not exists", Toast.LENGTH_SHORT).show();
            }else{
                try {
                    FileInputStream fileInputStream = new FileInputStream(sourceFile);
                    URL url = new URL(uploadServerUri);
                    // Apro una connessione HTTP con l'URL
                    conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true); // Allow Inputs
                    conn.setDoOutput(true); // Allow Outputs
                    conn.setUseCaches(false);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                    conn.setRequestProperty("uploaded_file", filename);

                    dos = new DataOutputStream(conn.getOutputStream());
                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"route_file\"; filename=\"" + filename + "\"" + lineEnd);

                    dos.writeBytes(lineEnd);

                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];
                    // leggo il file e scrivo nel DataOutputStream
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    while(bytesRead > 0){
                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }

                    //scrivo i dati multipart necessari dopo il contenuto del file
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    InputStream in = new BufferedInputStream(conn.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    String nextLine;
                    StringBuilder sb = new StringBuilder();
                    while ((nextLine = reader.readLine()) != null) {
                        sb.append(nextLine);
                    }


                    //verifico il codice di risposta del server
                    serverResponseCode = String.valueOf(conn.getResponseCode());

                    if(serverResponseCode.equals("200")){
                        Log.e("ResponseCode", serverResponseCode);
                        //Toast.makeText(GoalsActivity.this, "File upload complete", Toast.LENGTH_SHORT).show();
                    }

                    fileInputStream.close();
                    dos.flush();
                    dos.close();
                } catch (FileNotFoundException | MalformedURLException | ProtocolException e) {
                    progressBar.setVisibility(View.GONE);
                    e.printStackTrace();
                    Toast.makeText(GoalsActivity.this, e.getMessage() , Toast.LENGTH_SHORT).show();

                } catch (IOException e) {
                    progressBar.setVisibility(View.GONE);
                    e.printStackTrace();
                    Toast.makeText(GoalsActivity.this, e.getMessage() , Toast.LENGTH_SHORT).show();
                }
            }
            return serverResponseCode;
        }
    }
}