package com.oscararagon.revolutionbike.struct;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.oscararagon.revolutionbike.R;
import com.oscararagon.revolutionbike.classes.UserLogged;

import org.w3c.dom.Text;

public class AccountFragment extends Fragment {

    private TextView username;
    private TextView editProfilePic;
    private ImageView profilePicture;

    private static final int PERMISSION_REQUEST = 0;
    private static final int RESULT_LOAD_IMAGE = 1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View userView = inflater.inflate(R.layout.fragment_account, container, false);


        profilePicture = (ImageView) userView.findViewById(R.id.profile_picture);
        username = (TextView) userView.findViewById(R.id.Username);
        editProfilePic = (TextView) userView.findViewById(R.id.editPic);

        UserLogged usr = new UserLogged(getContext());
        username.setText(usr.getUsername());

        if(usr.getProfilePicture() != null){
            profilePicture.setImageResource(0);
            profilePicture.setBackgroundResource(0);
            profilePicture.setImageBitmap(BitmapFactory.decodeFile(usr.getProfilePicture()));
        }

        editProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                        v.getContext().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST);
                }

                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        return userView;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case PERMISSION_REQUEST:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getContext(), "Permesso accettato", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Permesso negato", Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode){
            case RESULT_LOAD_IMAGE:
                UserLogged usr = new UserLogged(getContext());
                if(resultCode == getActivity().RESULT_OK){
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATE_ADDED};
                    Cursor c = getContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePathColumn[0]);
                    String picturePath = c.getString(columnIndex);
                    c.close();
                    usr.saveProfilePicture(picturePath);
                    profilePicture.setImageResource(0);
                    profilePicture.setBackgroundResource(0);
                    profilePicture.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                }
        }
    }
}
