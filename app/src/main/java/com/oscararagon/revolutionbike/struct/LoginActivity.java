package com.oscararagon.revolutionbike.struct;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.oscararagon.revolutionbike.R;
import com.oscararagon.revolutionbike.classes.UserLogged;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class LoginActivity extends AppCompatActivity {

    static final int PASSATO = 10;

    private TextView signup;

    private TextInputEditText user, psw;
    private TextView afterLogin;
    private Button btnLogin;
    private String LOGIN_FAILED = "L'username e/o la password inseriti sono errati. Riprovare";
    private int userId;
    private String username, peso, altezza;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        user = findViewById(R.id.usrusr);
        psw = findViewById(R.id.pswrdd);
        btnLogin = findViewById(R.id.btnlogin);
        signup = findViewById(R.id.signup);
        afterLogin = findViewById(R.id.afterlogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userid = user.getText().toString();
                String password = psw.getText().toString();
                userid = userid.replaceAll(" ", "");
                String url_login = "https://www.revolutionbike.it/android_connection/login.php?email="+userid+"&password="+password;
                new DoLogin().execute(url_login);
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signUpIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.revolutionbike.it/login/registrazione.php"));
                if(signUpIntent.resolveActivity(getPackageManager()) != null){
                    startActivity(signUpIntent);
                }
            }
        });
    }


    public class DoLogin extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            String dati_utente = "";
            String url_login = strings[0];
            URL loginURL;
            {
                try {
                    loginURL = new URL(url_login);
                    HttpURLConnection client = (HttpURLConnection) loginURL.openConnection();
                    client.setRequestMethod("GET");

                    InputStream risposta = new BufferedInputStream(client.getInputStream());
                    dati_utente = provoLogin(risposta);
                } catch (MalformedURLException e) { //se non si crea correttamente l'oggetto URL
                    e.printStackTrace();
                } catch (IOException e) { //se la connessione non si apre
                    e.printStackTrace();
                }
            }
            return dati_utente;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s == "ok" ){
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }else{
                afterLogin.setTextColor(Color.RED);
                afterLogin.setText(LOGIN_FAILED);
            }
        }

        private String provoLogin(InputStream in) {
            StringBuilder sb = new StringBuilder();
            boolean success;
            String ris = "";

            try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
                String nextLine;
                while ((nextLine = reader.readLine()) != null) {
                    sb.append(nextLine);
                }
                JSONObject datiRisposta = new JSONObject(sb.toString());
                success = datiRisposta.getBoolean("success");
                if(success){
                    JSONArray datiBody = datiRisposta.getJSONArray("user");
                    userId = datiBody.getJSONObject(0).getInt(datiBody.getJSONObject(0).keys().next());
                    username = datiBody.getJSONObject(0).getString("User");
                    peso = datiBody.getJSONObject(0).getString("Peso");
                    altezza = datiBody.getJSONObject(0).getString("Altezza");
                    UserLogged u = new UserLogged(getApplicationContext());
                    u.saveUserData(userId, username, peso, altezza);
                    ris = "ok";
                }else{
                    userId = 0;
                    ris = datiRisposta.getString("message");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }catch (JSONException e) {
                e.printStackTrace();
            }
            return ris;
        }
    }
}
