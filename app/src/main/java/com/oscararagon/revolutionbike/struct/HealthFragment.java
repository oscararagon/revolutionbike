package com.oscararagon.revolutionbike.struct;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.oscararagon.revolutionbike.R;

public class HealthFragment extends Fragment{
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View healthView = inflater.inflate(R.layout.fragment_health, container, false);
        return healthView;
    }
}
