package com.oscararagon.revolutionbike.struct;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import android.provider.DocumentsContract;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.oscararagon.revolutionbike.R;
import com.oscararagon.revolutionbike.lib.HTTPConnectionClass;
import com.oscararagon.revolutionbike.lib.ResponseCallback;

import java.io.FileOutputStream;
import java.io.IOException;


import java.util.ArrayList;

public class ReportDetailActivity extends AppCompatActivity {

    private static final int REQUEST_READ_STORAGE = 1;
    private static final int REQUEST_WRITE_STORAGE = 1;
    private static final int WRITE_REQUEST_CODE = 10;


    private boolean mReadStoragePermissionGranted, mWriteStoragePermissionGranted;
    LineChart velGraph, pGraph, altiGraph;
    Bundle report;
    TextView titolo, km, durata, kcal, data, NumReport, avgVel, avgBPM, avgAlti;
    LinearLayout layoutGraph;
    Button PDFbutton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_detail);

        Intent intent = getIntent();

        titolo = (TextView) findViewById(R.id.report_title);
        NumReport = (TextView) findViewById(R.id.idReport);
        durata = (TextView) findViewById(R.id.durata2);
        km = (TextView) findViewById(R.id.km2);
        data = (TextView) findViewById(R.id.data2);

        layoutGraph = (LinearLayout) findViewById(R.id.layoutGraph);
        velGraph = (LineChart) findViewById(R.id.velocita);
        pGraph = (LineChart) findViewById(R.id.pressione);
        altiGraph = (LineChart) findViewById(R.id.altimetria);
        avgVel = (TextView) findViewById(R.id.avgVel);
        avgBPM = (TextView) findViewById(R.id.avgBPM);
        avgAlti = (TextView) findViewById(R.id.avgAlti);
        PDFbutton = (Button) findViewById(R.id.PDFbutton);

        PDFbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetStoragePermission(); //prima richiedo i permessi di lettura e scrittura sullo storage all'utente
                if(mReadStoragePermissionGranted && mWriteStoragePermissionGranted){
                    Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("application/pdf");
                    intent.putExtra(Intent.EXTRA_TITLE, titolo.getText().toString()+".pdf");
                    //intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, Context.DOWNLOAD_SERVICE);
                    startActivityForResult(intent, WRITE_REQUEST_CODE);
                }

            }
        });

        //prendo i dati del report specifico e li associo ai vari textview
        report = intent.getExtras();
        //recupero titolo e id
        titolo.setText(report.getString("Titolo").toUpperCase());
        NumReport.setText(Integer.toString(report.getInt("NumReport")));
        //recupero i dati del report
        durata.setText(report.getString("DurataSetted"));
        km.setText(report.getString("KmSetted")+" km");
        data.setText(report.getString("Data"));


        //da qui preleverò i dati da db dell'altitudine, pressione e velocità
        ArrayList<Bundle> URL= new ArrayList<>();
        Bundle CSV = new Bundle();
        int idReport = Integer.parseInt(report.getString("Id"));
        CSV.putString("URL", "https://revolutionbike.it/filesviaggi/"+idReport+"-dati.csv");
        URL.add(CSV);

        /**
        * RECUPERO DATI DAL CSV
        * primo dato di ogni lista è la media. I successivi elementi sono le medie ogni 10 minuti
        * RIGA 1 VELOCITA
        * RIGA 2 PRESSIONE
        * RIGA 3 ALTIMETRIA
        * */
        new HTTPConnectionClass(values -> {
            if(values != null){ //valori del viaggio terminato
                layoutGraph.setVisibility(View.VISIBLE);
                PDFbutton.setVisibility(View.VISIBLE);

                double avg;
                for(int j = 0; j < values.size(); j++){
                    ArrayList<Entry> valEntry = new ArrayList<>();
                    if(j == 0){
                        //array delle velocità
                        String[] velVal = values.get(j).getStringArray("velocita");
                        avg = Math.round(Double.parseDouble(velVal[1]) * 100d) / 100d;
                        avgVel.setText(Double.toString(avg)+" km/h");
                        for (int i=2; i < velVal.length; i++) {
                            float x = (i - 2f) + 10f;
                            float y = Math.round(Float.parseFloat(velVal[i]) * 100f) / 100f;
                            valEntry.add(new Entry(x, y));
                        }
                    }
                    if(j == 1){
                        //array del battito cardiaco
                        String[] bpmVal = values.get(j).getStringArray("pressione");
                        avg = Math.round(Double.parseDouble(bpmVal[1]) * 100d) / 100d;
                        avgBPM.setText(Double.toString(avg)+" bpm");
                        for (int i=2; i < bpmVal.length; i++) {
                            float x = (i - 2f) + 10f;
                            float y = Math.round(Float.parseFloat(bpmVal[i]) * 100f) / 100f;
                            valEntry.add(new Entry(x, y));
                        }
                    }
                    if(j == 2){
                        //array delle altimetrie
                        String[] altVal = values.get(j).getStringArray("altimetria");
                        avg = Math.round(Double.parseDouble(altVal[1]) * 100d) / 100d;
                        avgAlti.setText(Double.toString(avg)+" m");
                        for (int i=2; i < altVal.length; i++) {
                            float x = (i - 2f) + 10f;
                            float y = Math.round(Float.parseFloat(altVal[i]) * 100f) / 100f;
                            valEntry.add(new Entry(x, y));
                        }
                    }
                    LineDataSet set1 = new LineDataSet(valEntry, null);
                    // black lines and points
                    set1.setColor(Color.BLACK);
                    set1.setCircleColor(Color.BLACK);

                    // larghezza linea del grafico
                    set1.setLineWidth(1f);
                    set1.setCircleRadius(2f);

                    // disegno i punti del grafico come solid circles
                    set1.setDrawCircleHole(false);
                    set1.setDrawFilled(true);

                    // coloro l'area del grafico di blu
                    set1.setFillColor(Color.BLUE);

                    //creo il data object con il dataset
                    ArrayList<ILineDataSet> dataSets = new ArrayList<>();
                    dataSets.add(set1);

                    // creo il data object con i set di dati
                    LineData data = new LineData(dataSets);
                    if(j == 0) {
                        velGraph.setData(data);
                        velGraph.getAxisRight().setEnabled(false); //per non avere l'asse delle Y a destra, ma solo a sinistra
                        velGraph.animateX(1000);

                    }
                    else if (j == 1){
                        pGraph.setData(data);
                        pGraph.getAxisRight().setEnabled(false); //per non avere l'asse delle Y a destra, ma solo a sinistra
                        pGraph.animateX(1000);
                    }
                    else if (j == 2) {
                        altiGraph.setData(data);
                        altiGraph.getAxisRight().setEnabled(false); //per non avere l'asse delle Y a destra, ma solo a sinistra
                        altiGraph.animateX(1000);
                    }
                }
            }
        }).execute(URL);
    }

    /**
    * gestisco i permessi di lettura e scrittura su storage
    * */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_READ_STORAGE){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                //l'utente ha dato il permesso da popup: faccio scaricare
                mReadStoragePermissionGranted = true;
               // createPDF(titolo.getText().toString());
            }else{
                //permesso negato, quindi non posso creare il PDF
                mReadStoragePermissionGranted = false;
                Toast.makeText(this, "Permesso di leggere lo strage non concesso", Toast.LENGTH_SHORT).show();
            }
        }else if(requestCode == REQUEST_WRITE_STORAGE){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                mWriteStoragePermissionGranted = true;
            }else{
                //permesso negato, quindi non posso creare il PDF
                mWriteStoragePermissionGranted = false;
                Toast.makeText(this, "Permesso di leggere lo strage non concesso", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == WRITE_REQUEST_CODE){
            createPDF(data.getData());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Creo il PDF a partire dall'URI passato grazie all'intent CREATE_DOCUMENT
     * */
    private void createPDF(Uri pathFile){

        //creo il bitmap della view
        Bitmap graph = Bitmap.createBitmap(layoutGraph.getWidth(), layoutGraph.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(graph);
        layoutGraph.draw(c);

        //creo il documento PDF
        PdfDocument document = new PdfDocument();

        //Creo il PageInfo del documento
        PdfDocument.PageInfo pageInfo =
                new PdfDocument.PageInfo.Builder(layoutGraph.getWidth(), layoutGraph.getHeight(), 1).create();

        //inizializzo la pagina
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();
        canvas.drawBitmap(graph, 0, 0, new Paint());

        //termino la pagina
        document.finishPage(page);

       try {
           FileOutputStream fos = (FileOutputStream) getContentResolver().openOutputStream(pathFile);
           document.writeTo(fos);
            Toast.makeText(this, "PDF salvato con successo", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Qualcosa è andato storto:  " + e.toString(), Toast.LENGTH_LONG).show();
        }
        // chiudo il documento
        document.close();
    }

    /**
     * Richiedo i permessi di scrittura e lettura
     * nello storage all'utente
     * */
    private void GetStoragePermission(){
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            mReadStoragePermissionGranted = true;
        } else {
            //spiego all'utente, nel caso inizialmente non avesse dato il permesso, il perché sia ncessario darlo
            if(shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)){
                Toast.makeText(this, "Il permesso per l'accesso allo storage serve per salvare il PDF del report",
                        Toast.LENGTH_SHORT).show();
            }

            //richiedo il permesso per leggere da storage
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_READ_STORAGE);
        }

        if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            mWriteStoragePermissionGranted = true;
        }else{
            //spiego all'utente, nel caso inizialmente non avesse dato il permesso, il perché sia ncessario darlo
            if(shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)){
                Toast.makeText(this, "Il permesso per l'accesso allo storage serve per salvare il PDF del report",
                        Toast.LENGTH_SHORT).show();
            }

            //richiedo il permesso di scrivere sullo storage
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
        }
    }
}

