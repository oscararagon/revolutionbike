package com.oscararagon.revolutionbike.lib;

import android.os.Bundle;

import java.util.ArrayList;

public interface ResponseCallback {
    void onRespond(ArrayList<Bundle> values);
}
