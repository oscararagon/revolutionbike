package com.oscararagon.revolutionbike.lib;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.common.api.GoogleApiClient;
import com.oscararagon.revolutionbike.R;

import java.util.ArrayList;
import java.util.List;

public class ReportAdapter extends ArrayAdapter<Bundle> {



    //costruttore
    public ReportAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Bundle> objects) {
        super(context, resource, objects);
    }


    /**
    * Ritorno la ListView e i suoi elementi settati
    * */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.report_item_layout, parent, false);
        }

        ImageView imgReport;
        TextView date, km, kcal, duration, title;

        final Bundle report = getItem(position);

        imgReport = (ImageView) convertView.findViewById(R.id.imgReport);
        title = (TextView) convertView.findViewById(R.id.reportTitle);
        date = (TextView) convertView.findViewById(R.id.data);
        duration = (TextView) convertView.findViewById(R.id.durata);
        km = (TextView) convertView.findViewById(R.id.kilometri);
        kcal = (TextView) convertView.findViewById(R.id.kcal);

        //setterò i vari textview collegandomi al db
        imgReport.setImageResource(R.drawable.ic_bycicle);
        title.setText(report.getString("Titolo").toUpperCase());
        date.setText("Data: "+report.getString("Data"));
        duration.setText("Durata: "+report.getString("DurataSetted"));
        km.setText("Km: "+report.getString("KmSetted")+" km");
        kcal.setText("Kcal: "+report.getString("KcalSetted"));

        return convertView;
    }
}
