package com.oscararagon.revolutionbike.lib;

import android.os.AsyncTask;
import android.os.Bundle;


import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HTTPConnectionClass extends AsyncTask<ArrayList<Bundle>, String, ArrayList<Bundle>> {


    private ResponseCallback listener;

    public HTTPConnectionClass(ResponseCallback listener){ this.listener = listener; }

    @Override
    protected ArrayList<Bundle> doInBackground(ArrayList<Bundle>... params) {
        ArrayList<Bundle> dati = null;

        String url_passed = params[0].get(0).getString("URL"); //URL passato dalla classe
        URL requestURL;
        {
            /**
            * Ci sono due possibilità:
            * 1. Devo leggere il file CSV contenente i dati del report
            * 2. Devo leggere la risposta di un'interrogazione al dataabase
            *
            * In entrambi i casi ritorno un ArrayList<Bundle> contenente il corpo della risposta
            * */
            try {
                requestURL = new URL(url_passed);
                if(url_passed.contains(".csv")){
                    dati = ReadFIleCSV(requestURL);
                }else{
                    HttpURLConnection client = (HttpURLConnection) requestURL.openConnection();
                    client.setRequestMethod("GET");

                    InputStream risposta = new BufferedInputStream(client.getInputStream());
                    dati = resultToArray(risposta);
                }
            } catch (MalformedURLException e) { //se non si crea correttamente l'oggetto URL
                e.printStackTrace();
            } catch (IOException e) { //se la connessione non si apre
                e.printStackTrace();
            } catch (CsvException e) {
                e.printStackTrace();
            }
        }
        return dati;
    }


    @Override
    protected void onPostExecute(ArrayList<Bundle> ris) {
        super.onPostExecute(ris);
        listener.onRespond(ris);
    }

    private ArrayList<Bundle> resultToArray(InputStream in) {
        StringBuilder sb = new StringBuilder();
        boolean success;
        String msg;
        JSONArray jsonBody;
        Bundle b = new Bundle();
        ArrayList<Bundle> ris = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            String nextLine;
            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);
            }
            JSONObject queryResult = new JSONObject(sb.toString());
            success = queryResult.getBoolean("success");
            if(success){
                jsonBody = queryResult.getJSONArray("body");

                //salvo la risposta della pagina HTTP come array di bundle. Se la pagina .php è una insert, jsonbody avrà lunghezza 1
                if(jsonBody.get(0) instanceof JSONObject) { //Se è un array di Object allora è una select
                    for (int j = 0; j < jsonBody.length(); ++j) {
                        Iterator<String> keys = jsonBody.getJSONObject(j).keys();
                        Bundle el = new Bundle();
                        while(keys.hasNext()){
                            String key = keys.next();
                            String val = jsonBody.getJSONObject(j).getString(key);
                            el.putString(key, val);
                        }
                        ris.add(el);
                    }
                }else{ //altrimenti è un update, insert o delete e devo ritornare il messaggio di successo/errore
                    b.putString("msg", jsonBody.getString(0));
                    ris.add(b);
                }
            }else{
                msg = queryResult.getString("body");
                b.putString("msg", msg);
                ris.add(b);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return ris;
    }

    private ArrayList<Bundle> ReadFIleCSV(URL stockURL) throws IOException, CsvException {

        ArrayList<Bundle> dati = new ArrayList<>();
        Bundle v = new Bundle();    //lista dei valori di velocità
        Bundle a = new Bundle();    //lista dei valori di altimetria
        Bundle p = new Bundle();    //lista dei valori di pressione

        BufferedReader in = new BufferedReader(new InputStreamReader(stockURL.openStream()));
        CSVParser parser = new CSVParserBuilder().withSeparator(',').build();

        CSVReader reader = new CSVReaderBuilder(in).withCSVParser(parser).build(); //associo il builder al lettore del file CSV
        List<String[]> rows = reader.readAll();
        if(rows != null){
            v.putStringArray(rows.get(1)[0], rows.get(1));
            p.putStringArray(rows.get(2)[0], rows.get(2));
            a.putStringArray(rows.get(3)[0], rows.get(3));
            dati.add(v);
            dati.add(p);
            dati.add(a);
        }else
            dati = null;
        return dati;
    }
}
