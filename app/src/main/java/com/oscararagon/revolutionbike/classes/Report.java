package com.oscararagon.revolutionbike.classes;

import com.google.android.gms.maps.model.LatLng;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Report {

    private int ID;
    private String titolo;
    private LatLng partenza, arrivo;
    private String strPartenza;
    private String strArrivo;
    private String durata;
    private float km, kcal;
    private Date currentDate;



    public Report( String titolo, String strPartenza, String strArrivo, LatLng partenza, LatLng arrivo, float kcal, String durata){
        this.titolo = titolo;
        this.strPartenza = strPartenza;
        this.strArrivo = strArrivo;
        this.partenza = partenza;
        this.arrivo = arrivo;
        this.kcal = kcal;
        this.durata = durata;
    }


    public Report( String titolo, float km, float kcal, String durata){
        this.titolo = titolo;
        this.km = km;
        this.kcal = kcal;
        this.durata = durata;
    }


    public int getID() { return ID;}

    public void setID(int ID) {this.ID = ID; }

    public String getTitolo() { return titolo; }

    public String getStrPartenza() {
        return strPartenza;
    }

    public String getStrArrivo() {
        return strArrivo;
    }


    public String getCurrentDate() {
        DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        this.currentDate = new Date();
        return format.format(this.currentDate);
    }

    public LatLng getPartenza() {
        return partenza;
    }

    public LatLng getArrivo() {
        return arrivo;
    }

    public float getKcal() {
        return kcal;
    }

    public float getKm() {
        return km;
    }

    public String getDurata() {
        return durata;
    }
}
