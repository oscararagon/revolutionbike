package com.oscararagon.revolutionbike.classes;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import com.oscararagon.revolutionbike.R;

public class UserLogged {
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private static int ID;

    public UserLogged(Context mContext){
        sp = mContext.getSharedPreferences(mContext.getString(R.string.sharedpreferences_filename), Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    public void saveUserData(int id, String username, String peso, String altezza){
        editor.putInt("ID", id);
        editor.putString("username", username);
        editor.putString("peso", peso);
        editor.putString("altezza", altezza);
        editor.commit();
    }

    public void saveProfilePicture(String picPath){
        editor.putString("profilePicture", picPath);
        editor.commit();
    }

    public String getProfilePicture(){
        String returnedPic = null;
        if(sp.contains("profilePicture")){
            returnedPic = sp.getString("profilePicture", "");
        }
        return returnedPic;
    }

    //se il mio file SharedPreferences non contiene ancora l'ID dell'utente, ritorno 0
    public int getUserID(){
        int returnedID = 0;
        if(sp.contains("ID")){
            returnedID = sp.getInt("ID", 0);
        }
        return returnedID;
    }


    //se il mio file SharedPreferences non contiene ancora un username, ritorno null
    public String getUsername(){
        String returnedUser = null;
        if(sp.contains("username")){
            returnedUser = sp.getString("username", "");
        }
        return returnedUser;
    }

    public String getPeso(){
        String returnedPeso = null;
        if(sp.contains("peso")){
            returnedPeso = sp.getString("peso", "");
        }
        return returnedPeso;
    }
    public String getAltezza(){
        String returnedAltezza = null;
        if(sp.contains("altezza")){
            returnedAltezza = sp.getString("altezza", "");
        }
        return returnedAltezza;
    }


    //metodi che serviranno per i report ecc...
}
