
<?php
require("lib/MySQLlib.php");
require("lib/db-keys.php");

$sql = new MySQL($key->server, $key->username, $key->password);
$sql->setDB($key->database_name);
$sql->ConnectToDB();

if($_SERVER["REQUEST_METHOD"] === "GET"){

  $array = array();

  $query = "SELECT `IdViaggio`, DATE_FORMAT(`Data`, \"%d/%m/%Y\"), `Titolo`, `DurataSetted`, `KmSetted`, `CalorieSetted` FROM `Viaggi`
            INNER JOIN `DatiViaggio` ON `DatiViaggio`.`CodViaggio` = `Viaggi`.`IdViaggio`
            INNER JOIN `Persone` ON `Persone`.`Id` = `Viaggi`.`CodPersona`
            WHERE `CodPersona` = ? AND `StatoViaggio` = ?";

  $array[] = $_GET["CodPersona"];
  $array[] = $_GET["Stato"];
  $result = $sql->QLQuery($query, 'ii', $array);

  $response = array();


  if($result->num_rows > 0){ 
    $report = array();
    $response["success"] = true;
    $response["body"] = array();

    foreach($result as $row){
      $report["Id"] = $row[$result->fetch_fields()[0]->name];
      $report["Data"] = $row[$result->fetch_fields()[1]->name];
      $report["Titolo"] = $row[$result->fetch_fields()[2]->name];
      $report["DurataSetted"] = $row[$result->fetch_fields()[3]->name];
      $report["KmSetted"] = $row[$result->fetch_fields()[4]->name];
      $report["KcalSetted"] = $row[$result->fetch_fields()[5]->name];
      array_push($response["body"], $report);
    }
  }else{
    $response["success"] = false;
    $response["body"] = array();

    if($_GET["Stato"] == 0 ) array_push($response["body"], "Ancora nessun viaggio impostato!");
    else array_push($response["body"], "Ancora nessun viaggio terminato");
  }
  //qui ritorno il successo o meno dell'inserimento del viaggio su db
  echo json_encode($response);
}
?>
