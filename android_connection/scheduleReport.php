
<?php
require("lib/MySQLlib.php");
require("lib/db-keys.php");

$sql = new MySQL($key->server, $key->username, $key->password);
$sql->setDB($key->database_name);
$sql->ConnectToDB();

if($_SERVER["REQUEST_METHOD"] === "GET"){

  $array = array();
  $arrayQL = array();
  $arrayDati = array();
  $response = array();

  if($_GET["kmSetted"] == 0.0){ //impostato il percorso e poi gli obiettivi

    $queryViaggi="INSERT INTO `Viaggi` (`CodPersona`, `Titolo`, `PartenzaStr`, `ArrivoStr`, `PartenzaLat`, `PartenzaLon`, `ArrivoLat`, `ArrivoLon`, `Data`)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $params_viaggi = 'isssdddds';

    $array[] = $_GET["CodPersona"];
    $array[] = $_GET["titolo"];
    $array[] = $_GET["strPartenza"];
    $array[] = $_GET["strArrivo"];
    $array[] = $_GET["latPartenza"];
    $array[] = $_GET["lonPartenza"];
    $array[] = $_GET["latArrivo"];
    $array[] = $_GET["lonArrivo"];
    $array[] = $_GET["data"];

    $resultViaggio = $sql->DMLQuery($queryViaggi, $params_viaggi, $array);

    if(!$resultViaggio){ //DMLQuery ritorna false se non riesce a connettersi al db e fare il preparedstetement
      $response["success"] = false;
      $response["message"] = "Errore nella connessione al database";
    }else{ //DMLQuery ha inserito correttament il viaggio
      //recupero l'ID del viaggio appena inserito
      $query = "SELECT `IdViaggio` FROM `Viaggi` WHERE `Data` LIKE ? AND `CodPersona` LIKE ?";
      $arrayQL[] = $_GET["data"];
      $arrayQL[] = $_GET["CodPersona"];
      $result = $sql->QLQuery($query, 'si', $arrayQL);
      $cod = $sql->ResultToArray($result);
      $idViaggio = (int)$cod[0];

      //inserisco i dati del viaggio
      $queryDatiViaggi="INSERT INTO `DatiViaggio` (`CodViaggio`, `CalorieSetted`, `DurataSetted`)  VALUES (?, ?, ?)";
      $arrayDati[] = $idViaggio;
      $arrayDati[] = $_GET["kcalSetted"];
      $arrayDati[] = $_GET["durataSetted"];
      $params_dativiaggi = 'ids';
      $resultDati = $sql->DMLQuery($queryDatiViaggi, $params_dativiaggi, $arrayDati);

      $response["success"] = true;
      $response["body"] = array();
      array_push($response["body"], "Viaggio impostato correttamente!");
    }
  }else{ //Impostati gli obiettivi e non il percorso

    $queryViaggi="INSERT INTO `Viaggi` (`CodPersona`, `Titolo`, `Data`) VALUES (?, ?, ?)";
    $params_viaggi = 'iss';

    $array[] = $_GET["CodPersona"];
    $array[] = $_GET["titolo"];
    $array[] = $_GET["data"];

    $resultViaggio = $sql->DMLQuery($queryViaggi, $params_viaggi, $array);

    if(!$resultViaggio){ //DMLQuery non è riuscito a connettersi al db o non ha creato correttamente i preparedstatement
      $response["success"] = false;
      $response["message"] = "Errore nella connessione al database.";
    }else{ //DMLQuery ha aggiunto il viaggio correttamente
      //recupero l'ID del viaggio appena inserito
      $query = "SELECT `IdViaggio` FROM `Viaggi` WHERE `Data` LIKE ? AND `CodPersona` LIKE ?";
      $arrayQL[] = $_GET["data"];
      $arrayQL[] = $_GET["CodPersona"];
      $result = $sql->QLQuery($query, 'si', $arrayQL);
      $cod = $sql->ResultToArray($result);
      $idViaggio = (int)$cod[0];
      //inserisco i dati del viaggio
      $queryDatiViaggi = "INSERT INTO `DatiViaggio` (`CodViaggio`, `KmSetted`, `CalorieSetted`, `DurataSetted`) VALUES (?, ?, ?, ?)";
      $arrayDati[] = $idViaggio;
      $arrayDati[] = $_GET["kmSetted"];
      $arrayDati[] = $_GET["kcalSetted"];
      $arrayDati[] = $_GET["durataSetted"];
      $params_dativiaggi = 'idds';
      $resultDati = $sql->DMLQuery($queryDatiViaggi, $params_dativiaggi, $arrayDati);

      $response["success"] = true;
      $response["body"] = array();
      array_push($response["body"], "Viaggio impostato correttamente!");
    }
  }
  //qui ritorno il successo o meno dell'inserimento del viaggio su db
  echo json_encode($response);
}
?>
