<?php
require("lib/MySQLlib.php");
require("lib/db-keys.php");

$sql = new MySQL($key->server, $key->username, $key->password);
$sql->setDB($key->database_name);
$sql->ConnectToDB();

if($_SERVER["REQUEST_METHOD"] === "GET"){

  $query="SELECT `CodPersona` , `Username`, `Persone`.`Peso`, `Persone`.`Altezza` FROM `Utenti`
  INNER JOIN `Persone` ON `Persone`.`Id` = `Utenti`.`CodPersona`
   WHERE `Email` LIKE ? AND `Psw` LIKE ? AND `Attivo` = 1";
  $params_type = 'ss';

  $array=array();
  $array[] = $_GET['email'];
  $array[] = hash("sha256", $_GET['password']);



  $result = $sql->QLQuery($query, $params_type, $array);

  $response = array();

  if($result->num_rows > 0){
    $user = array();

    foreach($result as $row){
        $user["Id"] = $row[$result->fetch_fields()[0]->name];
        $user["User"] = $row[$result->fetch_fields()[1]->name];
        $user["Peso"] = $row[$result->fetch_fields()[2]->name];
        $user["Altezza"] = $row[$result->fetch_fields()[3]->name];
    }
    $response["user"] = array();
    array_push($response["user"], $user);
    $response["success"] = true;

  }else{
    $response["success"] = false;
      $response["Message"] = "Nessuna persona trovata";
  }

  echo json_encode($response);
}
?>
