
<?php
require("lib/MySQLlib.php");
require("lib/db-keys.php");

$sql = new MySQL($key->server, $key->username, $key->password);
$sql->setDB($key->database_name);
$sql->ConnectToDB();

if($_SERVER["REQUEST_METHOD"] === "GET"){

  $array = array();

  $query = "DELETE FROM `Viaggi` WHERE `CodPersona` = ? AND `IdViaggio` = ?";

  $array[] = $_GET["CodPersona"];
  $array[] = $_GET["IdReport"];
  $result = $sql->DMLQuery($query, 'ii', $array);

  $response = array();

    if(!$result){ //non si è stabilita la connessione al DB e non si è fatto il preparedstatement
        $response["success"] = false;
        $response["message"] = "Errore nella connessione al database";
    }else{//la query ha cancellato con successo il viaggio da DB
        $response["success"] = true;
        $response["body"] = array();
        array_push($response["body"], "Viaggio cancellato!");
    }

  //qui ritorno il successo o meno dell'inserimento del viaggio su db
  echo json_encode($response);
}
?>
