<?php
    // Veneri Luca 16/03/2018 5AI
    /* Classe per la gestione di MySQL in php */
    class MySQL{
	    // variabile che contine la connessione
		private $mysqli;
		// variabile che contiene il nome dell'host
		private $namehost;
		// variabie che contiene l'username di accesso al database
		private $username;
		// variabile che contine la password di accesso al database
		private $password;
		// variabile che contiene il nome del database
		private $DB;
		// variabile che contiene se la connessione è attiva o no
		private $connect = false;

		/* Costruttore della classe */
		function __construct($host, $user, $pass){
            $this->namehost = $host;
            $this->username = $user;
            $this->password = $pass;
		}

		/* Distruttore della classe */
		function __destruct(){}

		/* Creazione del messaggio di errore ottenuto durante la connessione
		 *
		 * Ritorna una stringa
		*/
        private function ConnectionError(){
            return '<p>Connection Error (' . $this->mysqli->connect_errno . ')<br />' . $this->mysqli->connect_error . '</p>';
        }

        /* Creazione del messaggio di errore ottenuto dalla classe mysqli
		 *
		 * Ritorna una stringa
		*/
        private function GeneralError(){
            return '<p>Errore (' . $this->mysqli->errno . ')<br />' . $this->mysqli->error . '</p>';
        }

		/* Creazione di una connessione a un database
		 *
         * Ritorna un boolean
		*/
		public function ConnectToDB(){
		    // controllo se esiste già una connessione
			if(!$this->connect){
			    // istanzio una connessione
				$this->mysqli = @new mysqli($this->namehost, $this->username, $this->password, $this->DB);
				// elimino le variabili importanti appena importate
				unset($this->namehost, $this->username, $this->password, $this->DB);
				// controllo se sono avvenuti degli erori durante la connessione
				if ($this->mysqli->connect_errno)
				    // faccio ritornare l'errore di connessione e blocco lo script
					die ($this->ConnectionError());
				// metto la connessione attiva (avvenuta con successo)
				$this->connect = true;
			}
			// ritorno se la connessione è attiva o no
			return $this->connect;
		}

        /* Creazione di una connessione a un database
         *
         * Ritorna un boolean
        */
        public function ConnectToHost(){
            // controllo se esiste già una connessione
            if(!$this->connect){
                // istanzio una connessione
                $this->mysqli = @new mysqli($this->namehost, $this->username, $this->password);
                // elimino le variabili importanti appena importate
                unset($this->namehost, $this->username, $this->password);
                // controllo se sono avvenuti degli erori durante la connessione
                if ($this->mysqli->connect_errno)
                    // faccio ritornare l'errore di connessione e blocco lo script
                    die ($this->ConnectionError());
                // metto la connessione attiva (avvenuta con successo)
                $this->connect = true;
            }
            // ritorno se la connessione è attiva o no
            return $this->connect;
        }

        /* Disconnessione al database
         *
         * Ritorna un boolean
        */
		public function Disconnect(){
            // controllo se esiste la connessione
			if($this->connect)
			    // chiudo la connessione
				if($this->mysqli->close())
				    // metto la connessione disattivata (non esistente)
					$this->connect = false;
			// ritorno se le operazioni sono avvenute con successo o no
			return !($this->connect);
		}

        /* DDL(Data Definition Language)
         *
         * Creazione di un database
         * Creazione e definizione di tabelle
         * Modifica del database
         *  - Aggiunta o eliminazione di una colonna da una tabella
         *  - Cancellazione di una tabella
         *  - Creare o eliminare indici
         *
         * Ritorna un boolean
        */
		public function DDLQuery($query){
            // variabile per il controllo del valore di ritorno del metodo
		    $turn = false;
		    // controllo se la connessione esiste e se il valore della query è diverso da null
			if($this->connect && $query != null){
			    // controllo se la query è avvenuta con successo
			    if(!$this->mysqli->query($query)){
			        // disconnetto il database
			        $this->Disconnect();
			        // fermo l'esecuzione dello script e viene stampato l'errore
			        die ($this->GeneralError());
                }
                // faccio ritornare il corretto successo del metodo
				$turn = true;
            }
            // ritorna il risultato del metodo
			return $turn;
		}

		/* DML(Data Manipulation Language)
		 *
		 * Inserimento di dati da una tabella
		 * Cancellazione di dati da una tabella
		 * Aggiornamento dei dati di una tabella
		 *
		 * 1° parametro contiene il preparato dalla query
		 *
		 * 2° parametro può contenere solo questi caratteri
		 *  - 's'	Corrisponde a variabili associate al tipo di dato stringa.
     *  - 'i'	Corrisponde a variabili associate al tipo di dato numeri interi.
     *  - 'd'	Corrisponde a variabili associate al tipo di dato numeri double.
     *  - 'b'	Corrisponde a variabili associate al tipo di dato BLOB, formato binario.
		 *
		 * 3° parametro è un array indicizzato che contine i parametri da mattere nel preparato
		 *
		 * Ritorna un boolean
		*/
        public function DMLQuery($prepere, $params_type, Array $params){
            // variabile per il controllo del valore di ritorno del metodo
            $turn = false;
            // controllo se la connessione esiste e se il valore del prepararto è diverso da null
            if($this->connect && $prepere != NULL) {
                // creo un preparato
                $stmt = $this->mysqli->prepare($prepere);
                // controllo se bisogna associare i parametri al preparato
                if(strlen($params_type) != 0) {
                    // creo l'array di parametri
                    $a_params = array();
                    // associo con un reference i tipi di parametri all'array che conterrà i parametri
                    $a_params[] = &$params_type;
                    // per usare la funzione call_user_func_array, i parametri devono essere passati come reference
                    for ($i = 0; $i < strlen($params_type); $i++)
                        $a_params[] = &$params[$i];
                    // uso la funzione call_user_func_array al posto di $stmt->bind_param() perché non accetta gli array come parametri
                    call_user_func_array(array($stmt, 'bind_param'), $a_params);
                }

                // esegue il preparato e controlla che non siano avvenuti degli errori
                if(!$stmt->execute()){
                    // disconnetto il database
                    $this->Disconnect();
                    // fermo l'esecuzione dello script e viene stampato l'errore
                    die('<p>Errore (' . $stmt->errno . ')<br />' . $stmt->error . '</p>');
                }
                // chiudo il preparato
                $stmt->close();
                // faccio ritornare il corretto successo del metodo
                $turn = true;
            }
            // ritorna il risultato del metodo
            return $turn;
        }

        /* QL(Query Language)
         *
         * Prelevare dati dal database
         *
         * 1° parametro contiene il preparato dalla query
		 *
		 * 2° parametro può contenere solo questi caratteri
		 *  - 's'	Corrisponde a variabili associate al tipo di dato stringa.
         *  - 'i'	Corrisponde a variabili associate al tipo di dato numeri interi.
         *  - 'd'	Corrisponde a variabili associate al tipo di dato numeri double.
         *  - 'b'	Corrisponde a variabili associate al tipo di dato BLOB, formato binario.
		 *
		 * 3° parametro è un array indicizzato che contine i parametri da mattere nel preparato
         *
         * Ritorna un boolean
        */
        public function QLQuery($prepere, $params_type, Array $params){
            // controllo se la connessione esiste e se il valore del prepararto è diverso da null
            if($this->connect && $prepere != null){
                // creo un preparato
                $stmt = $this->mysqli->prepare($prepere);
                // controllo se bisogna associare i parametri al preparato
                if(strlen($params_type) != 0) {
                    // creo l'array di parametri
                    $a_params = array();
                    // associo con un reference i tipi di parametri all'array che conterrà i parametri
                    $a_params[] = &$params_type;
                    // per usare la funzione call_user_func_array, i parametri devono essere passati come reference
                    for ($i = 0; $i < strlen($params_type); $i++)
                        $a_params[] = &$params[$i];
                    // uso la funzione call_user_func_array al posto di $stmt->bind_param() perché non accetta gli array come parametri
                    call_user_func_array(array($stmt, 'bind_param'), $a_params);
                }
                // esegue il preparato e controlla che non siano avvenuti degli errori
                if(!$stmt->execute()){
                    // disconnetto il database
                    $this->Disconnect();
                    // fermo l'esecuzione dello script e viene stampato l'errore
                    die('<p>Errore (' . $stmt->errno . ')<br />' . $stmt->error . '</p>');
                }
                // controllo se l'ottenimento del risultato è avvenuto correttamente
                if (!($result = $stmt->get_result())){
                    // disconnetto il database
                    $this->Disconnect();
                    // fermo l'esecuzione dello script e viene stampato l'errore
                    die('<p>Ottenimento result set fallito: (' . $stmt->errno . ')<br />' . $stmt->error . '</p>');
                }
                // chiudo il preparato
                $stmt->close();
                // ritorna il risultato del preparato
                return $result;
            }
            // ritorna falso se la condizione precedente non è stata verificata con successo
            return false;
        }

        /* Assegno il valore del database */
        public function setDB($DB){
            $this->DB = $DB;
        }

		/* Seleziona il database da utilizzare
		 *
		 * Ritorna un boolean
		*/
		public function SelectDatabase($nameDatabase){
            // variabile per il controllo del valore di ritorno del metodo
            $turn = false;
            // controllo se la connessione esiste e se il nome del database è diverso da null
            if($this->connect && $nameDatabase != null){
                // seleziono il database da utilizzare
                if(!$this->mysqli->select_db($nameDatabase)){
                    // disconnetto il database
                    $this->Disconnect();
                    // fermo l'esecuzione dello script e viene stampato l'errore
                    die($this->GeneralError());
                }
                // faccio ritornare il corretto successo del metodo
                $turn = true;
            }
            // ritorna il risultato del metodo
            return $turn;
        }

        /* Converte il valore di ritorno di una query in un array
        *  (converte solo la prima colonna)
        *
        *  Ritorna un array
        */
		public function ResultToArray($result){
            // variabile che conterrà tutti i valori del parametro
            $array = array();
            // controllo se la connessione è attiva e se result è diverso da null
            if($this->connect && $result != null){
                // controllo se nel rusult sono presenti dei record
                if($result->num_rows != 0) {
                    // faccio scorrete tutte le  righe
                    foreach ($result as $row)
                        // assegno alla posizione successiva nell'array il valore nella riga nella prima colonna
                        $array[] = $row[$result->fetch_fields()[0]->name];
                }
            }
            // ritorna il risultato del metodo
            return $array;
        }

        /* Genera una tabella con i valori di ritorno di una query
        *
        *  Ritorna un boolean
        */
		public function GenerateTable($result){
            // variabile contenente la tabella che viene ritornata
            $turn = "";
            // controllo se la connessione è attiva e se result è diverso da null
		    if($this->connect && $result != null){
		        // controllo se nel rusult sono presenti dei record
                if($result->num_rows == 0) {
                    $turn = "Nessun record.";
                } else {

                    $turn = "<table><thead><tr>";

                    $fields = $result->fetch_fields();

                    foreach ($fields as $column)
                        $turn .= "<th>" . $column->name . "</th>";

                    $turn .=  "</tr></thead><tbody>";

                    foreach ($result as $row) {
                        $turn .=  "<tr>";
                        foreach ($row as $field)
                            $turn .= "<td>" . $field . "</td>";
                        $turn .= "</tr>";
                    }

                    $turn .= "</tbody></table>";
                }
            }
            // ritorna il risultato del metodo
            return $turn;
        }
    }
